package pt.itsector.academia.myrates.core.contract;

import java.util.List;
import pt.itsector.academia.myrates.ui.model.CurrencyRateItem;

public interface LatestRatesContract {

  public interface LatestRatesPresenter {

    void fetchLatestRates();

    void openCurrencyRateDetails(String currency);

    void openCurrencyRateDetails(int pos);

    List<CurrencyRateItem> getRateItemList();

    String getBaseCurrency();

    String getDateString();

  }

  public interface LatestRatesView {

    void loadingStatus(boolean isLoadingNeeded);

    void listOfCurrencyRatesUpdated();

    void onError(int errorType, int errorMessage);

    void listVisibilityStatus(boolean isVisible);

    void navigateToDetails(String currency);
  }

}
