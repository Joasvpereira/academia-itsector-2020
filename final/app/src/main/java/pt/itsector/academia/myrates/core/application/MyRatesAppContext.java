package pt.itsector.academia.myrates.core.application;

import android.content.Context;

public interface MyRatesAppContext {

  Context getAppContext();

}
