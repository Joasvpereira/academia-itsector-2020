package pt.itsector.academia.myrates.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import pt.itsector.academia.myrates.R;
import pt.itsector.academia.myrates.core.repository.CurrencyRatesRepository;
import pt.itsector.academia.myrates.ui.rateslist.CurrencyListFragment;

public class EmptyActivity extends AppCompatActivity {


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    CurrencyListFragment fragment = new CurrencyListFragment();
    fragmentTransaction.replace(R.id.mainContainer, fragment).commit();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.option_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.action_settings) {
      String currency = new CurrencyRatesRepository().getBaseCurrency();
      Intent intent = new Intent(EmptyActivity.this, SettingsActivity.class);
      intent.putExtra(SettingsActivity.CURRENCY_SELECTED_KEY, currency);
      startActivityForResult(intent, SettingsActivity.REQUEST_CODE);
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    switch (requestCode) {
      case SettingsActivity.REQUEST_CODE: {
        settingsActivityResult(resultCode, data);
        break;
      }
      default:
        super.onActivityResult(requestCode, resultCode, data);
    }
  }

  private void settingsActivityResult(int resultCode, @Nullable Intent data) {
    if (resultCode == RESULT_OK) {
      if (data != null
          && data.getExtras() != null) {
        String currency = data.getExtras().getString(SettingsActivity.CURRENCY_SELECTED_KEY, "");
        if (!TextUtils.isEmpty(currency)) {
          new CurrencyRatesRepository().changeBaseCurrency(currency);
        }
      }
      //reload fragment
      FragmentManager fragmentManager = getSupportFragmentManager();
      FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
      CurrencyListFragment fragment = new CurrencyListFragment();
      fragmentTransaction.replace(R.id.mainContainer, fragment).commitAllowingStateLoss();
    }
  }
}
