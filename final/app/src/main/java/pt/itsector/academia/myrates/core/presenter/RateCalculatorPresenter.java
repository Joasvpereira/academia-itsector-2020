package pt.itsector.academia.myrates.core.presenter;

import java.util.Objects;
import pt.itsector.academia.myrates.App;
import pt.itsector.academia.myrates.R;
import pt.itsector.academia.myrates.core.contract.CalculatorContract;
import pt.itsector.academia.myrates.core.contract.CalculatorContract.ConversionType;
import pt.itsector.academia.myrates.core.contract.CalculatorContract.View;

public class RateCalculatorPresenter extends BasePresenter<CalculatorContract.View> implements
    CalculatorContract.Presenter {

  private String currencyFrom;
  private String currencyTo;
  private double rate = 0;

  public RateCalculatorPresenter(View view) {
    super(view);
  }

  @Override
  public String[] getCurrenciesAvailable() {
    return App.getInstance().getAppContext().getResources()
        .getStringArray(R.array.currency_entries);
  }

  @Override
  public void setCurrenciesToConvert(String currencyFrom, String currencyTo) {
    this.currencyFrom = currencyFrom;
    this.currencyTo = currencyTo;

    rate = 1.139;
  }

  @Override
  public void convertTo(ConversionType type, double value) {
    Objects.requireNonNull(type, "Missing conversion type!");

    double result = 0;

    if (ConversionType.BASE_TO_OTHER.equals(type)) {
      result = value * rate;
    } else {
      result = value / rate;
    }

    getView().onConversionResult(type, result);
  }
}
