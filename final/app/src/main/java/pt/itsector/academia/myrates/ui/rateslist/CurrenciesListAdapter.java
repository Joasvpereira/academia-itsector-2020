package pt.itsector.academia.myrates.ui.rateslist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import java.util.List;
import java.util.Objects;
import pt.itsector.academia.myrates.R;
import pt.itsector.academia.myrates.ui.model.CurrencyRateItem;

public class CurrenciesListAdapter extends Adapter<CurrenciesListViewHolder> {

  private CurrenciesListDelegate delegate;

  public CurrenciesListAdapter(
      CurrenciesListDelegate delegate) {
    Objects.requireNonNull(delegate,"CurrenciesListDelegate is required!");
    this.delegate = delegate;
  }

  @NonNull
  @Override
  public CurrenciesListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_currencies_list, parent, false);
    return new CurrenciesListViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull CurrenciesListViewHolder holder, int position) {
    CurrencyRateItem item = delegate.getItems().get(position);

    holder.currency.setText(item.getCurrency());
    holder.rate.setText(Double.toString(item.getRate()));
    holder.detailsIv.setOnClickListener(v -> delegate.positionClicked(position));
  }

  @Override
  public int getItemCount() {
    return delegate.getItems().size();
  }

  public interface CurrenciesListDelegate {
    List<CurrencyRateItem> getItems();
    void positionClicked(int pos);
  }
}
