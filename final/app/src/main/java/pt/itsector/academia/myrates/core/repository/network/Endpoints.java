package pt.itsector.academia.myrates.core.repository.network;

public class Endpoints {

  public static final String BASE_URL = "https://api.exchangeratesapi.io/";
  public static final String LATEST = "latest";

}
