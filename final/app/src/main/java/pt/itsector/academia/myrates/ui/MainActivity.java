package pt.itsector.academia.myrates.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import pt.itsector.academia.myrates.R;
import pt.itsector.academia.myrates.core.contract.LatestRatesContract;
import pt.itsector.academia.myrates.core.presenter.CurrenciesLatestRatesPresenter;
import pt.itsector.academia.myrates.ui.model.CurrencyRateItem;
import pt.itsector.academia.myrates.ui.rateslist.CurrenciesListAdapter;
import pt.itsector.academia.myrates.ui.rateslist.CurrenciesListAdapter.CurrenciesListDelegate;

public class MainActivity extends AppCompatActivity /*implements
    LatestRatesContract.LatestRatesView,
    CurrenciesListDelegate*/ {

  /*private TextView label;
  private RecyclerView currenciesRv;
  private LatestRatesContract.LatestRatesPresenter presenter;
  private CurrenciesListAdapter adapter;*/

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    //initViews();
    //presenter = new CurrenciesLatestRatesPresenter(this);
    //presenter.fetchLatestRates();
  }
/*
  private void initViews() {
    label = findViewById(R.id.labelTv);
    currenciesRv = findViewById(R.id.currencyRatesRv);

    adapter = new CurrenciesListAdapter(this);
    currenciesRv.setLayoutManager(new LinearLayoutManager(this));
    currenciesRv.setAdapter(adapter);
  }

  @Override
  public void loadingStatus(boolean isLoadingNeeded) {

  }

  @Override
  public void listOfCurrencyRates(List<CurrencyRateItem> currencyRateItemList) {
    StringBuilder textToDisplay = new StringBuilder();
    for (CurrencyRateItem item : currencyRateItemList) {
      textToDisplay.append(item.toString()).append("\n");
    }

    label.setText(textToDisplay);
    adapter.notifyDataSetChanged();
  }

  @Override
  public void onError(int errorType, int errorMessage) {
    label.setText("ERROR!!!");
  }

  @Override
  public void listVisibilityStatus(boolean isVisible) {
    if(isVisible){
      currenciesRv.setVisibility(View.VISIBLE);
      label.setVisibility(View.GONE);
    }else {
      currenciesRv.setVisibility(View.GONE);
      label.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void navigateToDetails(String currency) {

  }

  @Override
  public List<CurrencyRateItem> getItems() {
    return presenter.getRateItemList();
  }*/
}
