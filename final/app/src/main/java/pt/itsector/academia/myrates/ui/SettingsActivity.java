package pt.itsector.academia.myrates.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import pt.itsector.academia.myrates.R;
import pt.itsector.academia.myrates.core.repository.CurrencyRatesRepository;
import pt.itsector.academia.myrates.core.repository.Repository;

public class SettingsActivity extends AppCompatActivity {

  public static final int REQUEST_CODE = 845;
  public static final String CURRENCY_SELECTED_KEY = "currency";

  private RecyclerView recyclerView;
  private String[] currencies;
  private int selectedCurrencyPos;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.settings_activity);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }

    currencies = getResources().getStringArray(R.array.currency_entries);
    int pos = -1;
    String baseCurrency = null;
    Intent intent = getIntent();
    Bundle bundle = intent.getExtras();
    if(bundle != null){
      baseCurrency = bundle.getString(CURRENCY_SELECTED_KEY);
    }

    if(!TextUtils.isEmpty(baseCurrency)){
      for(int i = 0; i < currencies.length; i++){
        if(currencies[i].equalsIgnoreCase(baseCurrency)){
          pos = i;
          break;
        }
      }
    }

    BaseCurrencyAdapter adapter = new BaseCurrencyAdapter();
    adapter.setStartingPosition(pos);

    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    linearLayoutManager.scrollToPosition(pos);

    recyclerView = findViewById(R.id.currencyRv);
    recyclerView.setLayoutManager(linearLayoutManager);
    recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
    recyclerView.setAdapter(new BaseCurrencyAdapter());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    Intent data = new Intent();
    data.putExtra(CURRENCY_SELECTED_KEY,currencies[selectedCurrencyPos]);
    setResult(RESULT_OK, data);
    super.onBackPressed();
  }

  private class BaseCurrencyAdapter extends Adapter<SimpleViewHolder> {

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(android.R.layout.simple_list_item_1, parent, false);
      return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
      holder.textView.setText(currencies[position]);

      if (position == selectedCurrencyPos) {
        holder.itemView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
      } else {
        holder.itemView.setBackgroundColor(-1);
      }

      holder.itemView.setOnClickListener(v -> {
        int oldPosition = selectedCurrencyPos;
        selectedCurrencyPos = position;
        holder.itemView.setSelected(true);
        notifyItemChanged(oldPosition);
        notifyItemChanged(selectedCurrencyPos);
      });
    }

    @Override
    public int getItemCount() {
      return currencies.length;
    }

    public void setStartingPosition(int pos) {
      selectedCurrencyPos = pos;
    }
  }

  private class SimpleViewHolder extends ViewHolder {

    TextView textView;

    public SimpleViewHolder(View view) {
      super(view);
      textView = itemView.findViewById(android.R.id.text1);
    }
  }
}