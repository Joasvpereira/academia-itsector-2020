package pt.itsector.academia.myrates.ui.rateslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import pt.itsector.academia.myrates.R;
import pt.itsector.academia.myrates.core.contract.LatestRatesContract;
import pt.itsector.academia.myrates.core.presenter.CurrenciesLatestRatesPresenter;
import pt.itsector.academia.myrates.ui.calc.RateCalculatorFragment;
import pt.itsector.academia.myrates.ui.model.CurrencyRateItem;
import pt.itsector.academia.myrates.ui.rateslist.CurrenciesListAdapter.CurrenciesListDelegate;

public class CurrencyListFragment extends Fragment implements
    LatestRatesContract.LatestRatesView,
    CurrenciesListDelegate {

  private TextView label;
  private RecyclerView currenciesRv;
  private LatestRatesContract.LatestRatesPresenter presenter;
  private CurrenciesListAdapter adapter;
  private View rootView;
  private TextView dateTv;
  private TextView baseCurrencyTv;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter = new CurrenciesLatestRatesPresenter(this);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_currency_list, container, false);

    initViews();
    presenter.fetchLatestRates();

    return rootView;
  }

  private void initViews() {
    label = rootView.findViewById(R.id.labelTv);
    currenciesRv = rootView.findViewById(R.id.currencyRatesRv);
    baseCurrencyTv = rootView.findViewById(R.id.baseCurrencyTv);
    dateTv = rootView.findViewById(R.id.dateTv);

    adapter = new CurrenciesListAdapter(this);
    if (getContext() != null) {
      currenciesRv.setLayoutManager(new LinearLayoutManager(getContext()));
      DividerItemDecoration divider = new DividerItemDecoration(
          getContext(),
          LinearLayout.VERTICAL);
      currenciesRv.addItemDecoration(divider);
      currenciesRv.setAdapter(adapter);
    }
  }

  @Override
  public void loadingStatus(boolean isLoadingNeeded) {
    rootView.findViewById(R.id.loadingView)
        .setVisibility((isLoadingNeeded) ? View.VISIBLE : View.GONE);
  }

  @Override
  public void listOfCurrencyRatesUpdated() {
    baseCurrencyTv.setText(presenter.getBaseCurrency());
    dateTv.setText(presenter.getDateString());
    adapter.notifyDataSetChanged();
  }

  @Override
  public void onError(int errorType, int errorMessage) {
    label.setText("ERROR!!!");
  }

  @Override
  public void listVisibilityStatus(boolean isVisible) {
    if (isVisible) {
      currenciesRv.setVisibility(View.VISIBLE);
      label.setVisibility(View.GONE);
    } else {
      currenciesRv.setVisibility(View.GONE);
      label.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void navigateToDetails(String currency) {
    Toast.makeText(getContext(), currency, Toast.LENGTH_SHORT).show();
    FragmentManager fm = getActivity().getSupportFragmentManager();
    fm.beginTransaction()
        .replace(R.id.mainContainer, new RateCalculatorFragment())
        .addToBackStack(null)
        .commit();
  }

  @Override
  public List<CurrencyRateItem> getItems() {
    return presenter.getRateItemList();
  }

  @Override
  public void positionClicked(int pos) {
    presenter.openCurrencyRateDetails(pos);
  }
}
