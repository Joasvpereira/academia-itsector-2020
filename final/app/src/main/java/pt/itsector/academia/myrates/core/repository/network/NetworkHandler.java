package pt.itsector.academia.myrates.core.repository.network;

import android.text.TextUtils;
import android.widget.TextView;
import com.google.gson.Gson;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import org.json.JSONException;
import pt.itsector.academia.myrates.core.repository.model.LatestRates;

public class NetworkHandler {

  private static NetworkHandler _NetworkHandler;


  private NetworkHandler() {
  }

  public static NetworkHandler getInstance() {
    if (_NetworkHandler != null) {
      return _NetworkHandler;
    }
    return new NetworkHandler();
  }

  public LatestRates getLatestRates() throws IOException, JSONException {
    return getLatestRates(null);
  }

  public LatestRates getLatestRates(String baseRate) throws IOException, JSONException {
    HashMap<String, String> param = null;
    if(!TextUtils.isEmpty(baseRate)){
      param = new HashMap<>();
      param.put("base", baseRate);
    }
    URL latestRatesUrl = NetworkUtilities.buildUrl(Endpoints.LATEST, param);
    String resultAsString = NetworkUtilities.getResponseFromHttpUrl(latestRatesUrl);

    return new Gson().fromJson(resultAsString, LatestRates.class);
  }

}
