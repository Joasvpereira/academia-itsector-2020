package pt.itsector.academia.myrates.ui.rateslist;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import pt.itsector.academia.myrates.R;

public class CurrenciesListViewHolder extends ViewHolder {

  TextView currency;
  TextView rate;
  ImageView detailsIv;

  public CurrenciesListViewHolder(@NonNull View itemView) {
    super(itemView);

    currency = itemView.findViewById(R.id.currency);
    rate = itemView.findViewById(R.id.rate);
    detailsIv = itemView.findViewById(R.id.detailsIv);
  }
}
