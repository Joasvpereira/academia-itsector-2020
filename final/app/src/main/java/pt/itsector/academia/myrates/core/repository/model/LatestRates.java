package pt.itsector.academia.myrates.core.repository.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;

public class LatestRates {

  @Expose
  @SerializedName("date")
  private String date;
  @Expose
  @SerializedName("base")
  private String base;
  @Expose
  @SerializedName("rates")
  private HashMap<String,String> rates;

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getBase() {
    return base;
  }

  public void setBase(String base) {
    this.base = base;
  }

  public HashMap<String, String> getRates() {
    return rates;
  }

  public void setRates(HashMap<String, String> rates) {
    this.rates = rates;
  }

}
