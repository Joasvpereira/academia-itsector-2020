package pt.itsector.academia.myrates.core.repository.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import pt.itsector.academia.myrates.App;
import pt.itsector.academia.myrates.core.Config;

public class ShareData {

  public static final String SETTINGS_SHARED_PREFERENCES_FILE = "settings";
  public static final String SELECTED_CURRENCY_PROPERTY_SETTINGS = "selected_currency";

  public String getBaseCurrency() {
    SharedPreferences pref = loadSharedPreferences(SETTINGS_SHARED_PREFERENCES_FILE);

    return pref.getString(SELECTED_CURRENCY_PROPERTY_SETTINGS, Config.DEFAULT_CURRENCY);
  }

  public void setBaseCurrency(String currency) {
    SharedPreferences pref = loadSharedPreferences(SETTINGS_SHARED_PREFERENCES_FILE);
    Editor editor = pref.edit();
    editor.putString(SELECTED_CURRENCY_PROPERTY_SETTINGS, currency);
    editor.apply();
  }


  private SharedPreferences loadSharedPreferences(String name) {
    return App.getInstance().getAppContext()
        .getSharedPreferences(name, Context.MODE_PRIVATE);
  }


}
