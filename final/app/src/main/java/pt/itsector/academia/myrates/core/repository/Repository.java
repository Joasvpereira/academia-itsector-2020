package pt.itsector.academia.myrates.core.repository;

import pt.itsector.academia.myrates.core.repository.FetchDataTask.FetchWorkStates;
import pt.itsector.academia.myrates.core.repository.model.LatestRates;

public interface Repository {

  void fetchLatestRates(FetchWorkStates<LatestRates> callback);

  void fetchLatestRates(String baseCurrency, FetchWorkStates<LatestRates> callback);

  String getBaseCurrency();

  void changeBaseCurrency(String currency);

}
