package pt.itsector.academia.myrates.ui.model;

public class CurrencyRateItem {

  private String currency;
  private double rate;

  public CurrencyRateItem(String currency, double rate) {
    this.currency = currency;
    this.rate = rate;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public double getRate() {
    return rate;
  }

  public void setRate(double rate) {
    this.rate = rate;
  }

  @Override
  public String toString() {
    return "CurrencyRateItem{" +
        "currency='" + currency + '\'' +
        ", rate=" + rate +
        '}';
  }
}
