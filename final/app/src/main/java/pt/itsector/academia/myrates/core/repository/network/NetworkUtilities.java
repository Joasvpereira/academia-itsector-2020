package pt.itsector.academia.myrates.core.repository.network;

import android.net.Uri;
import android.net.Uri.Builder;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;
import javax.net.ssl.HttpsURLConnection;

public class NetworkUtilities {

  public static URL buildUrl(String route) {
    return buildUrl(route, null);
  }

  public static URL buildUrl(String route, HashMap<String, String> queryParameters) {

    String jsonUrl = Endpoints.BASE_URL + route;

    Builder builtUri = Uri.parse(jsonUrl).buildUpon();

    if (queryParameters != null) {
      for (String key : queryParameters.keySet()) {
        builtUri.appendQueryParameter(key, queryParameters.get(key));
      }
    }
    Uri uri = builtUri.build();

    URL url = null;

    try {
      url = new URL(uri.toString());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    return url;
  }


  public static String getResponseFromHttpUrl(URL url) throws IOException {

    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

    try {
      InputStream in = urlConnection.getInputStream();

      Scanner scanner = new Scanner(in);
      scanner.useDelimiter("\\A");

      boolean hasInput = scanner.hasNext();

      if (hasInput) {
        return scanner.next();
      } else {
        return null;
      }
    } finally {
      urlConnection.disconnect();
    }
  }
}