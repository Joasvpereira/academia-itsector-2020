package pt.itsector.academia.myrates;

import android.app.Application;
import android.content.Context;
import pt.itsector.academia.myrates.core.application.MyRatesAppContext;

public class App extends Application implements MyRatesAppContext {

  private static App INSTANCE;

  public static App getInstance(){
    return INSTANCE;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    INSTANCE = this;
  }

  @Override
  public Context getAppContext() {
    return getApplicationContext();
  }
}
