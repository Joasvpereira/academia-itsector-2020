package pt.itsector.academia.myrates.core.presenter;

import java.util.ArrayList;
import java.util.List;
import pt.itsector.academia.myrates.core.contract.LatestRatesContract.LatestRatesPresenter;
import pt.itsector.academia.myrates.core.contract.LatestRatesContract.LatestRatesView;
import pt.itsector.academia.myrates.core.repository.CurrencyRatesRepository;
import pt.itsector.academia.myrates.core.repository.FetchDataTask.FetchWorkStates;
import pt.itsector.academia.myrates.core.repository.Repository;
import pt.itsector.academia.myrates.core.repository.model.LatestRates;
import pt.itsector.academia.myrates.ui.model.CurrencyRateItem;

public class CurrenciesLatestRatesPresenter extends BasePresenter<LatestRatesView>
    implements LatestRatesPresenter {

  private Repository repo;

  private List<CurrencyRateItem> currencyRateItems;
  private String baseRate;
  private String date;


  public CurrenciesLatestRatesPresenter(LatestRatesView view,
      Repository repository) {
    super(view);
    this.repo = repository;
  }

  public CurrenciesLatestRatesPresenter(LatestRatesView view) {
    this(view, new CurrencyRatesRepository());
  }

  private List<CurrencyRateItem> transformLatestRatesInCurrencyRateItemList(
      LatestRates latestRates) {
    // TODO: 06/03/2020 create transformation.
    List<CurrencyRateItem> currencyRateItems = new ArrayList<>();
    for (String key : latestRates.getRates().keySet()) {

      if (key.equalsIgnoreCase(getBaseRateFromConfigs())) {
        continue;
      }

      CurrencyRateItem item;
      try {
        //noinspection ConstantConditions
        item = new CurrencyRateItem(key, Double.parseDouble(latestRates.getRates().get(key)));
        currencyRateItems.add(item);
      } catch (NullPointerException ex) {
        //skip item
      }
    }
    return currencyRateItems;
  }

  @Override
  public void fetchLatestRates() {
    repo.fetchLatestRates(getBaseRateFromConfigs(), new FetchWorkStates<LatestRates>() {
      @Override
      public void onStartWork() {
        getView().loadingStatus(true);
      }

      @Override
      public void onEndWork() {
        getView().loadingStatus(false);
      }

      @Override
      public void onSuccess(LatestRates latestRates) {
        getView().listVisibilityStatus(true);
        baseRate = latestRates.getBase();
        date = latestRates.getDate();
        currencyRateItems = transformLatestRatesInCurrencyRateItemList(
            latestRates
        );

        //returnValue.add(new CurrencyRateItem(latestRates.getBase(), 0));
        //getView().listOfCurrencyRates(returnValue);
        getView().listOfCurrencyRatesUpdated();
      }

      @Override
      public void onFail(Exception e) {
        // TODO: 06/03/2020 define errors
        getView().onError(1, -1);
      }
    });
  }

  @Override
  public void openCurrencyRateDetails(String currency) {
    getView().navigateToDetails(currency);
  }

  @Override
  public void openCurrencyRateDetails(int pos) {
    getView().navigateToDetails(currencyRateItems.get(pos).getCurrency());
  }

  @Override
  public List<CurrencyRateItem> getRateItemList() {
    if (currencyRateItems == null) {
      return new ArrayList<>();
    }
    return currencyRateItems;
  }

  @Override
  public String getBaseCurrency() {
    return baseRate;
  }

  @Override
  public String getDateString() {
    return date;
  }

  private String getBaseRateFromConfigs() {
    return repo.getBaseCurrency();
  }
}
