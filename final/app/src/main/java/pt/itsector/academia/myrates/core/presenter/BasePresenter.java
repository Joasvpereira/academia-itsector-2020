package pt.itsector.academia.myrates.core.presenter;

import androidx.annotation.NonNull;
import java.util.Objects;

public class BasePresenter<VIEW_CONTRACT> {

  private VIEW_CONTRACT view;

  public BasePresenter(VIEW_CONTRACT view) {
    setView(view);
  }

  public void attachContract(VIEW_CONTRACT view) {
    this.view = view;
  }

  protected VIEW_CONTRACT getView() {
    return view;
  }

  protected void setView(@NonNull VIEW_CONTRACT view) {
    Objects.requireNonNull(view, "View Contract must not be null");
    this.view = view;
  }

}
