package pt.itsector.academia.myrates.ui.calc;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.Objects;
import pt.itsector.academia.myrates.R;
import pt.itsector.academia.myrates.core.contract.CalculatorContract;
import pt.itsector.academia.myrates.core.contract.CalculatorContract.ConversionType;
import pt.itsector.academia.myrates.core.presenter.RateCalculatorPresenter;

public class RateCalculatorFragment extends Fragment implements CalculatorContract.View {

  Spinner spinner1;
  Spinner spinner2;
  EditText firstCurrencyEt;
  EditText secondCurrencyEt;
  private View rootView;
  private CalculatorContract.Presenter presenter;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter = new RateCalculatorPresenter(this);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_currency_calc, container, false);

    firstCurrencyEt = rootView.findViewById(R.id.firstCurrencyEt);
    secondCurrencyEt = rootView.findViewById(R.id.secondCurrencyEt);

    ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(
        Objects.requireNonNull(getContext()),
        android.R.layout.simple_spinner_dropdown_item,
        presenter.getCurrenciesAvailable()
    );

    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    //initViews();
    spinner1 = rootView.findViewById(R.id.firstCurrencySp);
    spinner2 = rootView.findViewById(R.id.secondCurrencySp);

    spinner1.setAdapter(adapter);
    spinner2.setAdapter(adapter);

    spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        presenter.setCurrenciesToConvert((String) spinner1.getSelectedItem(),
            (String) spinner2.getSelectedItem());
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    spinner2.setOnItemSelectedListener(spinner1.getOnItemSelectedListener());

    firstCurrencyEt.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (firstCurrencyEt.hasFocus()) {
          presenter.convertTo(ConversionType.BASE_TO_OTHER,
              TextUtils.isEmpty(s.toString()) ? 0 : Double.parseDouble(s.toString()));
        }
      }
    });

    secondCurrencyEt.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (secondCurrencyEt.hasFocus()) {
          presenter.convertTo(ConversionType.OTHER_TO_BASE,
              TextUtils.isEmpty(s.toString()) ? 0 : Double.parseDouble(s.toString()));
        }
      }
    });

    return rootView;
  }

  private void selectedCurrencyChangew(AdapterView<?> adapterView, View view, int i, long l) {
    presenter.setCurrenciesToConvert((String) spinner1.getSelectedItem(),
        (String) spinner2.getSelectedItem());
  }

  @Override
  public void onRateConvectionRateSuccess(double rate) {

  }

  @Override
  public void onConversionResult(ConversionType type, double resultValue) {
    switch (type) {
      case BASE_TO_OTHER:
        secondCurrencyEt.setText(String.valueOf(resultValue));
        break;
      default:
        firstCurrencyEt.setText(String.valueOf(resultValue));
    }
  }
}
