package pt.itsector.academia.myrates.core.repository;

import pt.itsector.academia.myrates.core.repository.FetchDataTask.FetchWorkStates;
import pt.itsector.academia.myrates.core.repository.local.ShareData;
import pt.itsector.academia.myrates.core.repository.model.LatestRates;
import pt.itsector.academia.myrates.core.repository.network.NetworkHandler;

public class CurrencyRatesRepository implements Repository {

  ShareData shareData;

  @Override
  public void fetchLatestRates(FetchWorkStates<LatestRates> callback) {
    FetchDataTask<Object, LatestRates> fetchDataTask = new FetchDataTask<>(
        parm -> NetworkHandler.getInstance().getLatestRates(),
        callback);

    fetchDataTask.execute();
  }

  @Override
  public void fetchLatestRates(String baseCurrency, FetchWorkStates<LatestRates> callback) {
    FetchDataTask<String, LatestRates> fetchDataTask = new FetchDataTask<>(
        parm -> NetworkHandler.getInstance().getLatestRates(baseCurrency),
        callback);

    fetchDataTask.execute();
  }

  @Override
  public String getBaseCurrency() {
    return getShareData().getBaseCurrency();
  }

  @Override
  public void changeBaseCurrency(String currency) {
    getShareData().setBaseCurrency(currency);
  }


  private ShareData getShareData() {
    if(shareData == null) shareData = new ShareData();
    return shareData;
  }
}
