package pt.itsector.academia.myrates.core.contract;

public class CalculatorContract {

  public enum ConversionType {
    BASE_TO_OTHER,
    OTHER_TO_BASE
  }

  public interface View {

    void onRateConvectionRateSuccess(double rate);

    void onConversionResult(ConversionType type, double resultValue);
  }

  public interface Presenter {

    String[] getCurrenciesAvailable();

    void setCurrenciesToConvert(String currencyFrom, String currencyTo);

    void convertTo(ConversionType type, double value);
  }

}
